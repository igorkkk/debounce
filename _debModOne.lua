--[[

-- usage
deb = require('_debModOne')
function dowork()
    print("Short pressed")
end
deb.set(3, dowork)
--]]

do
local function setnew()
    return {}
end
local M = {}
M.set = function(pin, work)
    gpio.mode(pin, gpio.INPUT, gpio.PULLUP)
    local o = setnew ()
    o.buttonPin = pin
    gpio.trig(pin)
	o.cicle = 1
    o.gotpress = false
    o.doshort = work
	o.getstate = 0
    o.startpin = function(self) 
        gpio.trig(self.buttonPin, "down", function(level)
            if not self.gotpress then
                self.gotpress = true
                local function exitnow(buf)
                    tmr.stop(buf); tmr.unregister( buf)
                    self.cicle, self.gotpress, self.getstate = 0, false, 0
                end
        		tmr.create():alarm(25, 1, function(buf)
                    if gpio.read(self.buttonPin) == self.getstate then
                        self.cicle = self.cicle + 1
                    else
                        self.cicle = self.cicle - 1
                        if self.cicle < 0 then exitnow(buf) end
                    end
                    if self.cicle > 3 and self.getstate == 0 then 
                        self.doshort()
                        self.getstate = 1
					elseif  self.cicle > 3 and self.getstate == 1 then 
						exitnow(buf)
					end 
                end)
            end
        end)
    end
	return o:startpin()
end
return M
end
