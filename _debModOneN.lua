-- Usage:
--[[
deb = require('_debModOneN')
function dowork()
    print("Short pressed")
end
deb.set(3, dowork)
--]]

local function setnew()
	return {}
end
local M = {}
M.set = function(pin, short)
	gpio.mode(pin, gpio.INPUT, gpio.PULLUP)
	local o = setnew ()
	o.buttonPin = pin
	gpio.trig(pin)
	o.cicle = 0
	o.startcount = false	
	o.gotpress = false
	o.doshort = short
	o.startpin = function(self) 
		gpio.trig(self.buttonPin, "down", function(level)
			if not self.gotpress then
				self.gotpress = true
				local function exitnow(buf)
					tmr.stop(buf); tmr.unregister( buf)
					self.cicle, self.gotpress, self.startcount = 0, false, false
				end
				tmr.create():alarm(50, 1, function(buf)
					if gpio.read(self.buttonPin) == 0 then
						self.cicle = self.cicle + 1
					else
						if not self.startcount then 
							self.cicle = self.cicle - 1
							if self.cicle < 0 then exitnow(buf) end
						else
							exitnow(buf)
						end
					end
					if self.cicle > 3 and not self.startcount then 
						self.startcount = true 
						self.doshort() 
					end	
				end)
			end
		end)
	end
	return o:startpin()
end
return M


